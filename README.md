# Todolist-Java



## LA TODOLIST

Faire une TodoList avec 2 classes, une Task et une TodoList et faire qu'on puisse ajouter des task à la liste, en supprimer et retirer toutes les tâches terminées
1. Créer une classe todo.Task qui ressemblera à ça : 
[![task-uid.png](https://gitlab.com/simplonlyon/promo14/todolist-java/-/raw/main/task-uuid.png)](https://example.com)


2. Faire une classe todo.TodoList qui sera composée d'une List de task ressemblant à ceci 
[![todolist](https://gitlab.com/simplonlyon/promo14/todolist-java/-/raw/main/todolist-java.png)]

3. Créer un constructeur dans la Task qui attendra un label et initialisera le done à false et mettra un uuid à l'intérieur de la propriété du même nom. Pour ce faire, vous pouvez utilisez la méthode random`UID() de la classe java.util.UUID
4. Dans la méthode addTask de la TodoList, utiliser le paramètre label pour créer une nouvelle instance de Task puis l'ajouter dans la liste de tasks
5. Dans la méthode clearDone(), faire une boucle sur toutes les tasks et retirer de la liste celles dont le done est à true
6. Créer une méthode actionTask(int index) dans TodoList qui va déclencher le toggleDone de la Task qui se trouve à l'index donné en paramètre


## Affichage de la TodoList

1. Créer dans la classe Task une méthode display() qui renverra une chaîne de caractère avec "☐ Label" dedans si la Task n'est pas done, et "☒ Label" si la Task est done
2. Dans la classe TodoList, créer une autre méthode display qui va :

    Boucler sur chaque Task contenu dans la liste
    A chaque tour de boucle, déclencher la méthode display de la Task
    Rajouter un ptit saut de ligne à la fin de chaque tâche ("\n" le saut de ligne)


3. Rajouter 2-3 task dans la todoList via le main si c'est pas déjà fait, et afficher le résultat du display dans la console pour voir le résultat


## Créer une User Interface avec un Scanner

1. Créer une classe ScannerTodoList qui aura en propriété une TodoList (initialisée dans le constructeur par exemple) et une méthode start()
2. Dans la méthode start, créer un scanner et faire une boucle 
3. À l'intérieur de cette boucle, on va afficher avec un sysout "Enter a new task" puis faire un nextLine pour récupérer un input utilisateur·ice et utiliser cette input dans la méthode addTask de la TodoList. Afficher ensuite le résultat avec la méthode display
4. Mettre le scanner en propriété de la la classe ScannerTodoList (pour pouvoir y accéder depuis d'autres méthodes), et créer une méthode addMenu, et dans cette méthode, on met le contenu du 3. dedans
5. A la place, dans le start, faire en sorte d'afficher les possibilités suivantes

```
1) Add a Task
2) Check task
3) Clear done
4) Exit
```

puis faire un nextInt() et si on récupère 1, alors on lance la méthode addMenu, si on récupère 4 alors on arrête la boucle et on ferme le scanner
6. Créer une méthode checkMenu qui va afficher l'état actuel de la todoList et demander "Which task do you want to check", et ensuite pareil, faire un nextInt() pour récupérer l'index de la task à cocher et déclencher un actionTask() avec. Rajouter un if dans le start qui déclenche le checkMenu
7. Et rajouter un dernier if dans le stat qui va déclencher le clearDone et afficher la todoList
(pas de correction à partir du 4. mais si on veut un truc complet, c'est un exemple de ce qu'il faudrait faire)

