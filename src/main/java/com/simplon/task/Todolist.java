package com.simplon.task;

import java.util.ArrayList;
import java.util.List;

public class Todolist {
    private List<Task> taskList = new ArrayList<>();

    public List<Task> getList(){
        return List.copyOf(taskList);
    }
    
    public void addtask(String label){
        Task task = new Task(label);
        taskList.add(task);
    }

    public String display(){
        String todolListString = "";
        for (Task task : taskList) {
            todolListString += task.display()+ "\n";
        }
        return todolListString;
    }

    public void actionTask (int index){
        taskList.get(index).toggleDOne();
    }

    public void clearDone(){
        for (int i = 0; i <taskList.size(); i++) {
            if(taskList.get(i).isDone()){
                taskList.remove(i);
                i--;
            }
        }
    }
}
