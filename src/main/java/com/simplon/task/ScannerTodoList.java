package com.simplon.task;

import java.util.Scanner;

public class ScannerTodoList {
    private Todolist todolist;
    private boolean over ;
    Scanner scanner = new Scanner(System.in);

    public ScannerTodoList() {
        this.todolist = new Todolist();
        this.over = false;
    }

    public void start() {
       
        System.out.println("Que voulez vous faire ?");
        System.out.println("1- Ajouter une tache");
        System.out.println("2- Checker une tache");
        System.out.println("3- Effacer les taches");
        System.out.println("4- Terminé");

        int choix = scanner.nextInt();

        if (choix == 1) {
            addMenu();
        }
        if (choix == 2) {
            checkMenu();
        }
        if (choix == 3) {
            // Pour tester on crée des taches
        todolist.addtask("Terminé mon dossier pro");
        todolist.addtask("Faire valider mon dossier pro");
        todolist.addtask("Envoyer mon dossier P");
        todolist.addtask("Dormir");

        todolist.actionTask(0);
        todolist.actionTask(1);
        todolist.actionTask(2);
            todolist.clearDone();
            System.out.println(todolist.display());
        }
        if (choix == 4) {
            over = false;
            System.out.println("Terminé");
        }
     
    }

    public void checkMenu() {
        // Pour tester on crée des taches
        todolist.addtask("Terminé mon dossier pro");
        todolist.addtask("Faire valider mon dossier pro");
        todolist.addtask("Envoyer mon dossier P");
        todolist.addtask("Dormir");
        int check = scanner.nextInt();
        todolist.actionTask(check);
        System.out.println(todolist.display());
        scanner.close();
    }


    public void addMenu() {
        while(!over){
            System.out.println("Enter a new task");
            String question = scanner.nextLine();
            todolist.addtask(question);
            System.out.println(todolist.display());

        }
        scanner.close();
    }


}
