package com.simplon.task;

import java.util.UUID;

public class Task {

    private String id;
    private String label;
    private boolean done;

    public Task(String label) {
        this.id = UUID.randomUUID().toString();
        this.label = label;
        this.done = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void toggleDOne(){
        done = !done;
    }

    
    public String display(){
        if(!done){
            return "✅" + label;
        }
        return "🚫" + label;
    }
    
}
